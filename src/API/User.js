import { domain } from '../constants.js'
import { getParams } from '../API/helper'

const Auth = async (nick, pass, endpoint) => {
  const url = `http://${domain}/${endpoint}`
  const params = getParams('POST', {
    nick: nick,
    password: pass
  })
  const rsp = await fetch(url, params)
  return {
    'status': rsp.status,
    'body': rsp.json()
  }
}

export { Auth }
