import { domain } from '../constants'
import { getParams } from '../API/helper'

const createOrder = async (name, src_add, dest_add) => {
  const url = `http://${domain}/customer/create_order`

  const params = getParams('POST', {
    'customer_name': name,
    'source_address': src_add,
    'destination_address': dest_add
  })
  let rsp = await fetch(url, params)
  if (rsp.status === 200) {
    rsp = rsp.json()
    return {
      'status': 'ok'
    }
  } else {
    return {
      'status': 'error',
      'msg': rsp.status
    }
  }
}

const getOrders = async (name) => {
  const url = `http://${domain}/customer/orders`
  const params = getParams('GET')

  let rsp = await fetch(url, params)
  if (rsp.status === 200) {
    rsp = rsp.json()
    return {
      'status': 'ok',
      'orders': rsp['orders']
    }
  } else {
    return {
      'status': 'error',
      'msg': rsp.status
    }
  }
}

export { getOrders, createOrder }
