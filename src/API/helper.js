export function getHeader (token) {
  return {
    'Content-Type': 'application/json; charset=utf-8',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
}
export function getParams (method, body) {
  console.log(body)
  return {
    method: method,
    mode: 'cors',
    headers: getHeader(),
    body: JSON.stringify(body)
  }
}
