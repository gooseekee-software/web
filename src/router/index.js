import Vue from 'vue'
import Router from 'vue-router'
import Auth from '../components/Auth'
import Start from '../components/Start'
import Order from '../components/Order'
import About from '../components/About'
import Deliveries from '../components/Deliveries'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/auth',
      name: 'Auth',
      component: Auth,
      props: {
        isLogin: true
      }
    },
    {
      path: '/register',
      name: 'Auth',
      component: Auth,
      props: {
        isLogin: false
      }
    },
    {
      path: '/',
      name: 'Start',
      component: Start
    },
    {
      path: '/order',
      name: 'Order',
      component: Order
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/deliveries',
      name: 'Deliveries',
      component: Deliveries
    }
  ]
})
